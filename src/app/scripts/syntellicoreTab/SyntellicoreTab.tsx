import * as React from "react";
import { Provider, Flex, Text, Button, Header, Input } from "@fluentui/react-northstar";
import TeamsBaseComponent, { ITeamsBaseComponentState } from "msteams-react-base-component";
import * as microsoftTeams from "@microsoft/teams-js";
import * as jwt from "jsonwebtoken";
/**
 * State for the syntellicoreTabTab React component
 */
export interface ISyntellicoreTabState extends ITeamsBaseComponentState {
    entityId?: string;
    name?: string;
    error?: string;
    syntellicore_url: string;
}

/**
 * Properties for the syntellicoreTabTab React component
 */
export interface ISyntellicoreTabProps {

}

/**
 * Implementation of the Syntellicore content page
 */
export class SyntellicoreTab extends TeamsBaseComponent<ISyntellicoreTabProps, ISyntellicoreTabState> {
    private mSyntellicoreUrlKey: string = "mSyntellicoreUrlKey";

    public async componentWillMount() {
        this.updateTheme(this.getQueryVariable("theme"));


        microsoftTeams.initialize(() => {
            microsoftTeams.registerOnThemeChangeHandler(this.updateTheme);
            microsoftTeams.getContext((context) => {
                this.setState({
                    entityId: context.entityId
                });
                this.updateTheme(context.theme);

                this.gotoSyntellicoreSite();

                microsoftTeams.authentication.getAuthToken({
                    successCallback: (token: string) => {
                        const decoded: { [key: string]: any; } = jwt.decode(token) as { [key: string]: any; };
                        this.setState({ name: decoded!.name });
                        microsoftTeams.appInitialization.notifySuccess();
                    },
                    failureCallback: (message: string) => {
                        this.setState({ error: message });
                        microsoftTeams.appInitialization.notifyFailure({
                            reason: microsoftTeams.appInitialization.FailedReason.AuthFailed,
                            message
                        });
                    },
                    resources: [process.env.SYNTELLICORE_APP_URI as string]
                });
            });
        });
    }

    /**
     * The render() method to create the UI of the tab
     */
    public render() {
        let lSyntellicoreUrl = localStorage.getItem(this.mSyntellicoreUrlKey);

        if (lSyntellicoreUrl) {
            return this.RenderUIForRegisteredSyntellicoreURL();
        }
        else {
            return this.RenderUIForRegisteringSyntellicoreURL();
        }
    }

    private RenderUIForRegisteringSyntellicoreURL() {
        return (
            <Provider theme={this.state.theme}>
                <Flex fill={true}>
                    <Flex.Item styles={{ padding: ".8rem 0 .8rem .5rem" }}>
                        <div>
                            <Header content="Syntellicore App Configuration" />
                            <Text content={`Hello ${this.state.name}, please add your Syntellicore URL to go forward.`} />

                            {this.state.error && <div><Text content={`An SSO error occurred ${this.state.error}`} /></div>}
                            <Input styles={{ padding: ".8rem 0 .8rem .5rem" }}
                                placeholder="Please enter your Syntellicore Url"
                                fluid
                                clearable
                                value={this.state.syntellicore_url}
                                onChange={(e, data) => {
                                    if (data) {
                                        this.setState({
                                            syntellicore_url: data.value
                                        });
                                    }
                                }}
                                required />
                            <Button onClick={() => this.saveSyntellicoreUrl()}>Save</Button>
                        </div>                        
                    </Flex.Item>
                </Flex>
            </Provider>
        );
    }

    private RenderUIForRegisteredSyntellicoreURL() {
        return (
            <Provider theme={this.state.theme}>
                <Flex fill={true} column styles={{
                    padding: ".8rem 0 .8rem .5rem"
                }}>
                    <Flex.Item>
                        <Header content="Syntellicore App" />
                    </Flex.Item>
                    <Flex.Item>
                        <div>
                            <div>
                                <Text content={`Welcome back ${this.state.name}, please wait while loading Syntellicore.`} />
                            </div>
                            {this.state.error && <div><Text content={`An SSO error occurred ${this.state.error}`} /></div>}
                        </div>
                    </Flex.Item>
                    <Flex.Item styles={{
                        padding: ".8rem 0 .8rem .5rem"
                    }}>
                        <Text size="smaller" content="(C) Copyright DW Dynamic Works LTD" />
                    </Flex.Item>
                </Flex>
            </Provider>
        );
    }


    private onSyntellicoreUrlValueChanged(event) {
        //this.setState(Object.assign({}, this.state, { value: event.target.value }));

        this.setState({
            syntellicore_url: event.target.value
        });
    }

    private saveSyntellicoreUrl() {
        let lUrl = this.state.syntellicore_url;

        // TODO: validate URL
        localStorage.setItem(this.mSyntellicoreUrlKey, lUrl);

        this.gotoSyntellicoreSite();
    }

    private gotoSyntellicoreSite() {
        let lSyntellicoreUrl = localStorage.getItem(this.mSyntellicoreUrlKey);

        if (lSyntellicoreUrl) {
            window.location.href = lSyntellicoreUrl+"?teams=1";
        }
    }
}
