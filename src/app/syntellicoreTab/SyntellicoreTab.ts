import { PreventIframe } from "express-msteams-host";

/**
 * Used as place holder for the decorators
 */
@PreventIframe("/syntellicoreTab/index.html")


export class SyntellicoreTab {
}
